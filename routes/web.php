<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->post('login', 'AuthController@login');
$router->post('register', 'AuthController@register');

$router->group(['middleware' => 'auth:api'], function ($app) {
    $app->post('uploadFile', 'UploadController@store');
    $app->get('/secured', function () {
        return response()->json([
            'message' => 'Hello!',
        ]);
    });
});

$router->get('/{route:.*}/', function () {
    return view('dashboard');
});
