<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UploadController extends Controller
{
    public function store(Request $request)
    {
        $user = Auth::user();
        $file = $request->file('filepond')->store(
            $user->uuid,
            's3'
        );
        
        File::create([
            'uuid' => (string) Str::uuid(),
            'path' => $file,
            'filename' => $request->file('filepond')->getClientOriginalName(),
            'user_uuid' => $user->uuid
        ]);
        
        return Auth::user();
    }
}
