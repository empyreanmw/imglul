import Vue from "vue";
import Router from "vue-router";

import Upload from "./components/upload";
import Settings from "./components/settings";
import Dashboard from "./components/dashboard";
import Images from "./components/images";
import Login from "./components/login";
import Logout from "./components/logout";
import Register from "./components/register";

Vue.use(Router);

export default new Router({
    mode: "history",
    base: "/",
    routes: [
        {
            path: "/dashboard",
            component: Dashboard,
            children: [
                { path: "/upload", component: Upload },
                { path: "/settings", component: Settings },
                { path: "/images", component: Images }
            ]
        },
        { path: "/", redirect: "/dashboard" },
        {
            path: "/login",
            name: "login",
            component: Login
        },
        {
            path: "/logout",
            name: "logout",
            component: Logout
        },
        {
            path: "/register",
            name: "register",
            component: Register
        }
    ]
});
