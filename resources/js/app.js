import Vue from "vue";
import router from "./router";
import VueResource from "vue-resource";
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import vueFilePond from "vue-filepond";
import "filepond/dist/filepond.min.css";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css";

Vue.use(BootstrapVue);
Vue.use(VueResource);
Vue.component("login", require("./components/login.vue"), {
    name: "login"
});
Vue.component("login-form", require("./components/login-form.vue"), {
    name: "login-form"
});
Vue.component("navbar", require("./components/navbar.vue"), {
    name: "login-form"
});

new Vue({
    router,
    components: {
        FilePond: vueFilePond(FilePondPluginImagePreview)
    }
}).$mount("#app");
